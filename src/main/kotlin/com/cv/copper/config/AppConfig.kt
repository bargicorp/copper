package com.cv.copper.config

import com.cv.copper.player.PlayerController
import com.cv.copper.player.PlayerDto
import com.cv.copper.player.PlayerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener


@Configuration
class AppConfig : WebMvcConfigurer {

    @Autowired
    private lateinit var playerService: PlayerService

    override fun addCorsMappings(registry: CorsRegistry) {
        super.addCorsMappings(registry)
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("POST","GET","PUT","PATCH","DELETE")
    }

    @EventListener(ApplicationReadyEvent::class)
    fun startPlayer() {
        val id = PlayerController.id
        if (playerService.findById(id) == null) {
            playerService.create(PlayerDto(id))
            println("Player created with id $id")
        } else {
            println("Player with id $id found in Database")
        }
    }
}
