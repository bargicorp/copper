package com.cv.copper.artist

import com.cv.copper.track.Track
import com.cv.copper.track.TrackController
import com.cv.copper.track.TrackRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn
import org.springframework.hateoas.mvc.ResourceAssemblerSupport
import org.springframework.stereotype.Component

@Component
class ArtistResourceAssembler : ResourceAssemblerSupport<Artist, ArtistResource>(Artist::class.java, ArtistResource::class.java){

    @Autowired
    private lateinit var trackRepository: TrackRepository

    override fun toResource(artist: Artist): ArtistResource {
        val resource = ArtistResource(
            artist.id,
            artist.name,
            trackRepository.countAllByArtists(artist)
        )
        resource.createdDate = artist.createdDate
        resource.lastModifiedDate = artist.lastModifiedDate
        artist.id?.let {
            resource.add(linkTo(methodOn(TrackController::class.java).findByArtist(it, null, null, null)).withRel("tracks"))
            resource.add(linkTo(methodOn(ArtistController::class.java).findById(it)).withRel("self"))
        }
        return resource
    }
}
