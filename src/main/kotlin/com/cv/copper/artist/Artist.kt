package com.cv.copper.artist

import com.cv.copper.Auditable
import com.cv.copper.track.Track
import org.hibernate.annotations.GenericGenerator
import org.springframework.data.annotation.CreatedDate
import javax.persistence.*
import java.io.Serializable

@Entity
@Table(name = "Artists")
data class Artist(
        @Id
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
        @Column(length = 36, name = "artist_id")
        var id: String?,
        @Column
        var name: String?,
        @ManyToMany(mappedBy = "artists")
        var tracks: MutableList<Track> = mutableListOf(),
        @Column
        var trackCount: Long? = 0
) : Auditable(), Serializable{
        override fun toString(): String {
                return "Artist(id=$id, name=$name)"
        }
}
