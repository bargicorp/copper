package com.cv.copper.artist

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query

interface ArtistRepository : JpaRepository<Artist, String> {

    fun findByName(name: String): Artist?
    fun findAllByNameContains(p: Pageable, query: String): Page<Artist>
    @Query("SELECT * FROM db_copper.artists ORDER BY RAND() LIMIT ?1 ;", nativeQuery = true)
    fun findRandom(number: Long): List<Artist>

}
