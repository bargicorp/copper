package com.cv.copper.artist

import com.cv.copper.release.Release
import com.cv.copper.track.Track
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.io.Serializable

interface ArtistService : Serializable {

    fun findByNameOrCreate(name: String): Artist
    fun findAll(p: Pageable): Page<Artist>
    fun findAll(p: Pageable, query: String): Page<Artist>
    fun findById(id: String): Artist?
    fun create(name: String): Artist
    fun getArtistsByReleaseId(id: String): List<Artist>
    fun updateTrackCount(artist: Artist)
    fun findRandom(p: Pageable, amount: Long): Page<Artist>

}
