package com.cv.copper.artist

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("", produces = [MediaTypes.HAL_JSON_VALUE])
class ArtistController {

    @Autowired
    private lateinit var artistResourceAssembler: ArtistResourceAssembler
    @Autowired
    private lateinit var artistService: ArtistService

    @GetMapping("/artists")
    fun findAll(@RequestParam(value = "query", required = false) query: String?, pageable: Pageable, assembler: PagedResourcesAssembler<Artist>): PagedResources<ArtistResource> {
        val trackPage = if (query == null) artistService.findAll(pageable) else artistService.findAll(pageable, query)
        return assembler.toResource(trackPage, artistResourceAssembler)
    }
    @GetMapping("/artists/random/{number}")
    fun findRandom(@PathVariable("number") number: Long, pageable: Pageable, assembler: PagedResourcesAssembler<Artist>): PagedResources<ArtistResource> {
        //return artistResourceAssembler.toResources(artistService.findRandom(number))
        val artistPage = artistService.findRandom(pageable, number)
        return assembler.toResource(artistPage, artistResourceAssembler)
    }
    @GetMapping("/artists/{artistId}")
    fun findById(@PathVariable("artistId") artistId: String): ResponseEntity<ArtistResource> {
        val artist = artistService.findById(artistId)
        artist?.let { return ResponseEntity.ok(artistResourceAssembler.toResource(artist)) }
                ?: return ResponseEntity.notFound().build()
    }
}
