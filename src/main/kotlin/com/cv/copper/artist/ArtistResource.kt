package com.cv.copper.artist

import com.cv.copper.resources.HALResource
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.core.Relation


@Relation(collectionRelation = "artists")
data class ArtistResource (
        @JsonProperty(value = "id")
        var aid: String?,
        var name: String?,
        var trackCount: Long?
): HALResource()
