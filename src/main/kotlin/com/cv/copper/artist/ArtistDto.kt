package com.cv.copper.artist

import java.io.Serializable

data class ArtistDto (
        var id: String?,
        var name: String?
) : Serializable
