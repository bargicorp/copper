package com.cv.copper.artist

import com.cv.copper.release.Release
import com.cv.copper.release.ReleaseRepository
import com.cv.copper.track.TrackRepository
import com.cv.copper.track.TrackService
import kotlinx.coroutines.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class ArtistServiceImpl : ArtistService {

    @Autowired
    private lateinit var artistRepository: ArtistRepository
    @Autowired
    private lateinit var trackService: TrackService
    @Autowired
    private lateinit var trackRepository: TrackRepository
    private lateinit var updateLater: Job

    override fun findByNameOrCreate(name: String): Artist {
        var artist = artistRepository.findByName(name)
        return artist ?: create(name)
    }
    override fun create(name: String): Artist {
        return artistRepository.save(Artist(null, name))
    }

    override fun findAll(p: Pageable): Page<Artist> {
        val artists = artistRepository.findAll(p)
        return artistRepository.findAll(p)
    }
    override fun findAll(p: Pageable, query: String): Page<Artist> {
        return artistRepository.findAllByNameContains(p, query)
    }

    override fun findById(id: String): Artist? {
        return artistRepository.findById(id).orElse(null)
    }

    override fun getArtistsByReleaseId(id: String): List<Artist> {
        val tracks = trackService.findByReleaseId(id)
        val artists: MutableList<Artist> = mutableListOf()
        for (track in tracks) {
            track?.artists?.let {
                for (artist in it) {
                    artists.add(artist)
                }
            }
        }
        val sort = hashMapOf<Artist, Int>()
        artists.forEach{sort[it] = sort[it]?.inc() ?: 1}
        val result = sort.toList().sortedBy { (_, value) -> value }.toMap()
        return result.keys.toList()
    }
    /**
     * This function uses a delay to wait for more updates to collect.
     * This behaviour reduces the amount of data base accesses when multiple files are uploaded at once
     * this function also creates redundant information, so it should not be used any longer
     * @param artist artist entity to count related track entities and update
     **/
    override fun updateTrackCount(artist: Artist) {
        // cancel the coroutine below when it is already waiting
        if (::updateLater.isInitialized && updateLater.isActive) {
            updateLater.cancel()
        }
        GlobalScope.async {
            // (re)start, effectively resetting the timer
            updateLater = launch {
                delay(3000L)
                // only executed when the coroutine is not cancelled
                if (isActive) {
                    artist.trackCount = trackRepository.countAllByArtists(artist)
                    artistRepository.save(artist)
                    println("Updated track count of artist " + artist.id + "(" + artist.name + ") to " + artist.trackCount)
                }
            }
        }
    }

    override fun findRandom(p: Pageable, amount: Long): Page<Artist> {
        val list = artistRepository.findRandom(amount)
        return PageImpl<Artist>(list, p, list.size.toLong())
    }
}
