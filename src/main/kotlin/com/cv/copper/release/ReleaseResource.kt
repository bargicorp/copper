package com.cv.copper.release

import com.cv.copper.resources.HALResource
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.core.Relation
import java.util.*

@Relation(collectionRelation = "releases")
data class ReleaseResource (
        @JsonProperty(value = "id")
        var rid: String?,
        var name: String?,
        var pictureUri: String?,
        var year: String?,
        var label: String?
): HALResource()
