package com.cv.copper.release

import com.cv.copper.artist.ArtistResourceAssembler
import com.cv.copper.artist.ArtistService
import com.cv.copper.track.TrackController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.hateoas.mvc.ResourceAssemblerSupport
import org.springframework.stereotype.Component

@Component
class ReleaseResourceAssembler : ResourceAssemblerSupport<Release, ReleaseResource>(Release::class.java, ReleaseResource::class.java) {

    @Autowired
    private lateinit var artistService: ArtistService
    @Autowired
    private lateinit var artistResourceAssembler: ArtistResourceAssembler

    override fun toResource(release: Release): ReleaseResource {

        val resource =  ReleaseResource(
                release.id,
                release.name,
                release.pictureUri,
                release.year,
                release.label
        )
        resource.createdDate = release.createdDate
        resource.lastModifiedDate = release.lastModifiedDate
        release.id?.let {
            val artists = artistService.getArtistsByReleaseId(it)
            var relString = "artist"
            if (artists.size > 1) {
                relString = "artists"
            }
            for (artist in artists) resource.embed(relString, artistResourceAssembler.toResource(artist))
            resource.add(linkTo(methodOn(ReleaseController::class.java).findById(it)).withRel("self"))
            resource.add(linkTo(methodOn(ReleaseController::class.java).findCoverArt(it)).withRel("coverArt"))
            resource.add(linkTo(methodOn(TrackController::class.java).findByRelease(it)).withRel("tracks"))
        }
        return resource
    }
}
