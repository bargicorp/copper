package com.cv.copper.release

import java.io.Serializable

class ReleaseDto (

        var id: String,
        var name: String?,
        var pictureUri: String?,
        var year: String?,
        var label: String?

): Serializable
