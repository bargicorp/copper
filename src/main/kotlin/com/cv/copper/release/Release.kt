package com.cv.copper.release


import com.cv.copper.Auditable
import com.cv.copper.track.Track
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*
import java.time.Year

@Entity
@Table(name = "Releases")
data class Release (
        @Id
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
        @Column(length = 36)
        var id: String?,
        @Column
        var name: String?,
        @Column
        var pictureUri: String?,
        @Column
        @OneToMany(mappedBy = "release")
        var tracks: List<Track>? = mutableListOf(),
        @Column
        var year: String?,
        @Column
        var label: String?,
        @Lob
        @Column(length = 66000)
        var coverArt: String? = null
) : Auditable() {
        override fun toString(): String {
                return "Release(id=$id, name=$name, year=$year)"
        }

}
