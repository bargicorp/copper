package com.cv.copper.release

import com.cv.copper.resources.HALResource
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.core.Relation

@Relation(collectionRelation = "releases")
data class CoverArtResource (
        @JsonProperty(value = "releaseId")
        var rid: String?,
        var base64encodedBinary: String?
): HALResource()
