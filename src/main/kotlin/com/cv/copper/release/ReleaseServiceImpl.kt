package com.cv.copper.release

import com.cv.copper.artist.Artist
import com.cv.copper.artist.ArtistRepository
import com.cv.copper.track.TrackRepository
import com.cv.copper.track.TrackService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional



@Service
@Transactional
class ReleaseServiceImpl : ReleaseService {

    @Autowired
    private lateinit var trackService: TrackService

    @Autowired
    private lateinit var releaseRepository: ReleaseRepository

    @Autowired
    private lateinit var artistRepository: ArtistRepository

    @Autowired
    private lateinit var trackRepository: TrackRepository

    override fun findAll(p: Pageable): Page<Release> {
        return releaseRepository.findAll(p)
    }

    override fun findAll(p: Pageable, query: String): Page<Release> {
        return releaseRepository.findAllByNameContains(p, query)
    }

    override fun findById(id: String): Release? {
        return releaseRepository.findById(id).orElse(null)
    }

    override fun create(dto: ReleaseDto): Release {
        return releaseRepository.save(Release(
                null,
                dto.name,
                dto.pictureUri,
                null,
                dto.year,
                dto.label
        ))
    }
    override fun create (name: String): Release {
        val release = releaseRepository.save(Release(
                null,
                name,
                null,
                null,
                null,
                null
        ))
        return release
    }
    override fun findByName(name: String): Release? {
        return releaseRepository.findByName(name)
    }

    override fun findByNameOrCreate(name: String): Release {
        return releaseRepository.findByName(name) ?: (create(name))
    }

    override fun findByArtistId (p: Pageable, artistId: String): Page<Release> {
        val list = releaseRepository.findByArtistId(artistId)
        return PageImpl<Release>(list, p, list.size.toLong())
    }

    override fun findRandom(p: Pageable, amount: Long): Page<Release> {
        val list = releaseRepository.findRandom(amount)
        return PageImpl<Release>(list, p, list.size.toLong())
    }
}
