package com.cv.copper.release

import com.cv.copper.track.Track
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query
import java.util.*

interface ReleaseRepository : JpaRepository<Release, String> {

    fun findByName(name: String): Release?
    fun findAllByNameContains(p: Pageable, query: String): Page<Release>
    @Query("SELECT * FROM db_copper.releases INNER JOIN (SELECT distinct tracks.release_id AS releaseId"
            + " FROM db_copper.tracks INNER JOIN db_copper.tracks_artists"
            + " ON tracks.track_id = tracks_artists.tracks_track_id"
            + " AND tracks_artists.artists_artist_id = ?1) releaseIds"
            + " WHERE id = releaseId"
            + " ORDER BY releases.year DESC;", nativeQuery = true)
    fun findByArtistId(artistId: String): List<Release>
    @Query("SELECT * FROM db_copper.releases ORDER BY RAND() LIMIT ? ;", nativeQuery = true)
    fun findRandom(number: Long): List<Release>
}
