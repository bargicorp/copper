package com.cv.copper.release

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/", produces = [MediaTypes.HAL_JSON_VALUE])
class ReleaseController {
    @Autowired
    private lateinit var releaseService: ReleaseService
    @Autowired
    private lateinit var releaseResourceAssembler: ReleaseResourceAssembler
    @Autowired
    private lateinit var coverArtResourceAssembler: CoverArtResourceAssembler

    @PostMapping("/releases")
    fun create(@RequestBody dto: ReleaseDto): ResponseEntity<ReleaseResource> {
        return ResponseEntity.ok(releaseResourceAssembler.toResource(releaseService.create(dto)))
    }
    @GetMapping("/releases")
    fun findAll(@RequestParam(value = "query", required = false) query: String?, pageable: Pageable, assembler: PagedResourcesAssembler<Release>): PagedResources<ReleaseResource> {
        val releasePage = if (query == null) releaseService.findAll(pageable) else releaseService.findAll(pageable, query)
        return assembler.toResource(releasePage, releaseResourceAssembler)
    }
    @GetMapping("/releases/{releaseId}")
    fun findById(@PathVariable("releaseId") releaseId: String): ResponseEntity<ReleaseResource> {
        val release = releaseService.findById(releaseId)
        release?.let { return ResponseEntity.ok(releaseResourceAssembler.toResource(release))} ?: return ResponseEntity.notFound().build()
    }
    @GetMapping("/artists/{artistId}/releases")
    fun findAllByArtistId(@PathVariable("artistId")artistId: String, pageable: Pageable, assembler: PagedResourcesAssembler<Release>): PagedResources<ReleaseResource> {
        val releasePage = releaseService.findByArtistId(pageable, artistId)
        return assembler.toResource(releasePage, releaseResourceAssembler)
    }
    @GetMapping("/releases/{releaseId}/cover")
    fun findCoverArt(@PathVariable("releaseId") releaseId: String): ResponseEntity<CoverArtResource> {
        val release = releaseService.findById(releaseId)
        release?.let { return ResponseEntity.ok(coverArtResourceAssembler.toResource(it))} ?: return ResponseEntity.notFound().build()
    }
    /*@GetMapping("/releases/random/{number}")
    fun findRandom(@PathVariable("number") number: Long): List<ReleaseResource> {
        return releaseResourceAssembler.toResources(releaseService.findRandom(number))
    }*/
    @GetMapping("/releases/random/{number}")
    fun findRandom(@PathVariable("number") number: Long, pageable: Pageable, assembler: PagedResourcesAssembler<Release>): PagedResources<ReleaseResource> {
        //return releaseResourceAssembler.toResources(releaseService.findRandom(number))
        val releasePage = releaseService.findRandom(pageable, number)
        return assembler.toResource(releasePage, releaseResourceAssembler)
    }

}
