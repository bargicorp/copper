package com.cv.copper.release

import org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.hateoas.mvc.ResourceAssemblerSupport
import org.springframework.stereotype.Component

@Component
class CoverArtResourceAssembler : ResourceAssemblerSupport<Release, CoverArtResource>(Release::class.java, CoverArtResource::class.java) {

    override fun toResource(release: Release): CoverArtResource {

        val resource = CoverArtResource (
                release.id,
                release.coverArt
        )
        resource.createdDate = release.createdDate
        resource.lastModifiedDate = release.createdDate
        release.id?.let {
            resource.add(linkTo(methodOn(ReleaseController::class.java).findById(it)).withRel("release"))
            resource.add(linkTo(methodOn(ReleaseController::class.java).findCoverArt(it)).withRel("self"))
        }
        return resource
    }
}
