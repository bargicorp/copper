package com.cv.copper.release

import com.cv.copper.artist.Artist
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.io.Serializable

interface ReleaseService : Serializable {
    fun findAll(p: Pageable): Page<Release>
    fun findAll(p: Pageable, query: String): Page<Release>
    fun findByName(name: String): Release?
    fun findById(id: String): Release?
    fun create(dto: ReleaseDto): Release
    fun create(name: String): Release
    fun findByNameOrCreate(name: String) : Release
    fun findByArtistId(p: Pageable, artistId: String) : Page<Release>
    fun findRandom(p: Pageable, amount: Long): Page<Release>
}
