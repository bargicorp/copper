package com.cv.copper

import com.sun.javafx.application.PlatformImpl
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CopperApplication

	fun main(args: Array<String>) {
		PlatformImpl.startup {}
		runApplication<CopperApplication>(*args)
	}


