package com.cv.copper.file

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.web.multipart.MultipartFile
import java.io.Serializable
import java.nio.file.Paths

interface FileService : Serializable {
    fun create(file: FileEntity): FileEntity?
    fun findAll(p: Pageable): Page<FileEntity>
    fun findAll(p: Pageable, query: String): Page<FileEntity>
    fun handleUpload(multipartFile: MultipartFile): FileEntity?

    companion object {
        val savePath = Paths.get("files/music")
    }
}
