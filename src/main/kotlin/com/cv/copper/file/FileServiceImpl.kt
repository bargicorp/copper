package com.cv.copper.file

import com.cv.copper.artist.Artist
import com.cv.copper.artist.ArtistService
import com.cv.copper.release.ReleaseRepository
import com.cv.copper.release.ReleaseService
import com.cv.copper.track.Track
import com.cv.copper.track.TrackService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Transactional
@Service
class FileServiceImpl (
        val executorService: ExecutorService = Executors.newSingleThreadExecutor(),
        val files: List<MultipartFile> = listOf()
): FileService {
    @Autowired
    private lateinit var fileRepository: FileRepository
    @Autowired
    private lateinit var releaseRepository: ReleaseRepository
    @Autowired
    private lateinit var artistService: ArtistService
    @Autowired
    private lateinit var releaseService: ReleaseService
    @Autowired
    private lateinit var trackService: TrackService

    override fun create(file: FileEntity): FileEntity? {
        return fileRepository.save(file)
    }

    override fun findAll(p: Pageable): Page<FileEntity> {
        return fileRepository.findAll(p)
    }

    override fun findAll(p: Pageable, query: String): Page<FileEntity> {
        return fileRepository.findAllByFilename(p, query)
    }
    override fun handleUpload(multipartFile: MultipartFile): FileEntity? {
        val response: FileEntity?
        val filename = multipartFile.originalFilename
        var fileEntity = FileEntity(filename ?: "temp")
        if (FileEntity.Extension.values().contains(fileEntity.extension)) {
            try {
                val oldPath = fileEntity.path
                File(FileService.savePath.resolve("").toUri()).mkdirs()
                Files.copy(multipartFile.inputStream, oldPath, StandardCopyOption.REPLACE_EXISTING)
                fileEntity.extractHeader()
                // The executor service forces following processes to wait until it's function is completed
                // used to prevent overflowing database requests
                executorService.execute(fun() {
                    fileEntity = create(fileEntity) ?: fileEntity
                    val artists = mutableListOf<Artist>()
                    fileEntity.id3header.artist?.let {
                        for (oneArtist in it) {
                            val a = artistService.findByNameOrCreate(oneArtist)
                            // artistService.updateTrackCount(a)
                            artists.add(a)
                        }
                    }
                    val release = fileEntity.id3header.album?.let { releaseService.findByNameOrCreate(it) }
                    release?.let {
                        it.coverArt = fileEntity.id3header.coverArt
                        it.year = fileEntity.id3header.year
                        it.label = fileEntity.id3header.label
                        releaseRepository.save(it)
                    }
                    trackService.createSave(Track(
                        "",
                        fileEntity.id3header.title?.let {it} ?: filename,
                        fileEntity.duration,
                        null,
                        fileEntity.id3header.trackNo?.let {Integer.parseInt(it)},
                        mutableListOf(fileEntity),
                        artists,
                        release,
                        null
                    ))
                })
                File(FileService.savePath.resolve(fileEntity.directory).toUri()).mkdirs()
                Files.move(oldPath, FileService.savePath.resolve(fileEntity.path), StandardCopyOption.REPLACE_EXISTING)
                response = fileEntity
            } catch (e: IOException) {
                throw Exception("Failed to store files $filename", e)
            }
        } else {
            response = null
        }
        return response
    }
}
