package com.cv.copper.file


import com.cv.copper.resources.HALResource
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.core.Relation

@Relation(collectionRelation = "files")
data class FileResource (
        @JsonProperty(value = "id")
        var fid: String?,
        var filename: String?,
        var path: String,
        @JsonProperty(value = "duration[s]")
        var duration: Long?,
        var iD3Header: FileEntity.ID3Header
        ): HALResource()