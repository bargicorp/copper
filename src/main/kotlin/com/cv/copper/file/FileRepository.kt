package com.cv.copper.file

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository


interface FileRepository : JpaRepository<FileEntity, String> {

    fun findAllByFilename(p: Pageable, query: String) : Page<FileEntity> {
        return findAllByFilename(p, query)
    }
}