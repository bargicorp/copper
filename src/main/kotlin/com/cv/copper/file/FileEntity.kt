package com.cv.copper.file

import com.cv.copper.Auditable
import com.cv.copper.track.Track
import com.cv.copper.utility.Constants
import org.apache.commons.io.FilenameUtils
import org.hibernate.annotations.GenericGenerator
import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.tag.FieldKey
import org.springframework.util.Base64Utils
import java.io.Serializable
import javax.persistence.*
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths
import java.time.Duration

@Entity
@Table(name = "Files")
data class FileEntity(
        @Transient
        val filename: String,

        @Id
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
        @Column(length = 36)
        var id: String? = null,
        var extension: Extension? = null,
        @Convert(converter = PathConverter::class)
        var path: Path? = null,
        @Convert(converter = PathConverter::class)
        var directory: Path? = null,
        var duration: Duration? = null,
        @Lob
        @Column(length = 66000)
        var id3header: ID3Header = ID3Header(),
        @ManyToOne(fetch = FetchType.EAGER)
        var track: Track? = null


) : Auditable(), Serializable {
    init {
        extension = Extension.valueOf(FilenameUtils.getExtension(filename).toUpperCase())
        path = FileService.savePath.resolve(filename)
    }
    fun sanitizeAndSplitTag(string: String) : List<String> {
        // This sequence is used to replace null-characters
        // which some tools use to separate multiple values from each other
        var sanitizedString = string.replace(0.toChar(), Constants.defaultTagSeparator)
        for (separator in Constants.knownTagSeparators) {
            sanitizedString = string.replace(separator.toRegex(), Constants.defaultTagSeparator.toString())
        }
        return sanitizedString.split(Constants.defaultTagSeparator)
        // TODO sanitize leading and trailing whitespaces
    }

    fun extractHeader(){
        val audioFile = AudioFileIO.read(File(path?.toUri()))
        duration = Duration.ofSeconds(audioFile.audioHeader.trackLength.toLong())
        audioFile.tag?.let {
            id3header.title = it.getFirst(FieldKey.TITLE).takeIf { s -> s.isNotBlank() }
            id3header.artist = sanitizeAndSplitTag(it.getFirst(FieldKey.ARTIST))
            id3header.album = it.getFirst(FieldKey.ALBUM).takeIf { s -> s.isNotBlank() }
            id3header.trackNo = it.getFirst(FieldKey.TRACK).takeIf { s -> s.isNotBlank() }
            id3header.year = it.getFirst(FieldKey.YEAR).takeIf { s -> s.isNotBlank() }
            id3header.label = it.getFirst(FieldKey.RECORD_LABEL).takeIf {s -> s.isNotBlank() }
            it.firstArtwork?.let {artwork -> id3header.coverArt = Base64Utils.encodeToString(artwork.binaryData)}
            generatePath()
        }
        // TODO add sanitizers for any tag to remove illegal characters and whitespace

    }
    private fun generatePath() {
        var path = ""
        val extension = if (extension != null) "." + extension?.string else ""
        id3header.artist?.let {
            path += "$it/"
        }
        id3header.year?.let {
            path += it
            path += if (id3header.album != null) pathSeparator else "/"
        }
        id3header.album?.let { path += "$it/"}
        directory = Paths.get(path)
        id3header.trackNo?.let {
            if (it.length == 1) path += "0"
            path += it
            path += if (id3header.title != null) pathSeparator else extension
        }
        id3header.title?.let { path += it + extension} ?: run {path += filename}
        this.path = Paths.get(path)
    }

    override fun toString(): String {
        return "FileEntity(id=$id, extension=$extension, path=$path)"
    }

    enum class Extension(val string: String){
        MP3("mp3")
    }
    class ID3Header (
        var title: String? = null,
        var artist: List<String>? = null,
        var album: String? = null,
        var trackNo: String? = null,
        var year: String? = null,
        var coverArt: String? = null,
        var label: String? = null
        )  : Serializable {
        override fun toString(): String {
            return "ID3Header(title=$title, artist=$artist, album=$album, trackNo=$trackNo, year=$year)"
        }
    }
    class PathConverter : AttributeConverter<Path, String> {
        override fun convertToDatabaseColumn(path: Path?): String {
            return path.toString()
        }

        override fun convertToEntityAttribute(path: String?): Path {
            return Paths.get(path)
        }
    }

    companion object {
        const val pathSeparator = (" - ")
    }
}
