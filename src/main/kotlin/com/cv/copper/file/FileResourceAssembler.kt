package com.cv.copper.file

import com.cv.copper.track.TrackResourceAssembler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.mvc.ResourceAssemblerSupport
import org.springframework.stereotype.Component
import java.time.Duration

@Component
class FileResourceAssembler : ResourceAssemblerSupport<FileEntity, FileResource>(FileEntity::class.java, FileResource::class.java) {

    @Autowired
    private lateinit var trackResourceAssembler: TrackResourceAssembler

    override fun toResource(file: FileEntity): FileResource {
        val resource = FileResource(
                file.id,
                file.filename,
                file.path.toString(),
                file.duration?.seconds,
                file.id3header
        )
        resource.createdDate = file.createdDate
        resource.lastModifiedDate = file.lastModifiedDate
        file.track?.let { resource.embed("track", trackResourceAssembler.toResource(it)) }
        return resource
    }
}
