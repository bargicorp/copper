package com.cv.copper.file
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("", produces = [MediaTypes.HAL_JSON_VALUE])
class FileController {

    @Autowired
    private lateinit var fileService: FileService
    @Autowired
    private lateinit var fileResourceAssembler: FileResourceAssembler
    @PostMapping("/files")
    fun uploadFile(@RequestParam(value = "file") multipartFile: MultipartFile, pageable: Pageable) : ResponseEntity<Any> {
        return ResponseEntity.ok(fileService.handleUpload(multipartFile) ?: ResponseEntity.notFound())
    }
    @GetMapping("/files")
    fun findAll(@RequestParam(value = "query", required = false) query: String?, pageable: Pageable, assembler: PagedResourcesAssembler<FileEntity>): PagedResources<FileResource> {
        val filePage = if (query == null) fileService.findAll(pageable) else fileService.findAll(pageable, query)
        return assembler.toResource(filePage, fileResourceAssembler)
    }
}
