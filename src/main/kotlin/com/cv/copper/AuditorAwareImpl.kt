package com.cv.copper

import org.springframework.data.domain.AuditorAware
import java.util.*


// Author Naresh Joshi, 10.02.2017
// SOURCE: https://www.programmingmitra.com/2017/02/automatic-spring-data-jpa-auditing-saving-CreatedBy-createddate-lastmodifiedby-lastmodifieddate-automatically.html

class AuditorAwareImpl : AuditorAware<String> {
    override fun getCurrentAuditor(): Optional<String> {
        // use this return value for user information in auditing.
        // (createdBy, modifiedBy)
        // such fields are currently not implemented
        return Optional.empty()
    }
}
