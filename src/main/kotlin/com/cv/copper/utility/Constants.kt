package com.cv.copper.utility

class Constants {
    companion object {
        const val defaultTagSeparator = ';'
        val knownTagSeparators = arrayOf(" / ")
    }
}
