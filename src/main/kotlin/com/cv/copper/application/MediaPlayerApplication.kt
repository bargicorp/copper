package com.cv.copper.application

import com.cv.copper.file.FileService
import com.cv.copper.player.PlayerService
import com.cv.copper.track.Track
import javafx.application.Application
import javafx.scene.media.Media
import javafx.scene.media.MediaPlayer
import javafx.stage.Stage
import javafx.util.Duration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MediaPlayerApplication : Application (){

    @Autowired
    private lateinit var playerService: PlayerService

    lateinit var mediaPlayer: MediaPlayer
    var status: MediaPlayer.Status? = null
    get() = try { mediaPlayer.status } catch (e: UninitializedPropertyAccessException) {null}

            override fun start(stage: Stage){
    }
    private fun initSave(function: (() -> Unit)){
        if (::mediaPlayer.isInitialized) {function()} else println("MediaPlayer not initialized")
    }
    fun playTrack(track: Track, time: Double = 0.0){
        if (::mediaPlayer.isInitialized) { mediaPlayer.stop()}
        MediaPlayer(Media(FileService.savePath.resolve(track.files[0]?.path)?.toUri().toString())).let {
            mediaPlayer = it
            mediaPlayer.seek(Duration(time))
            mediaPlayer.onEndOfMedia = Runnable { playerService.next() }
            mediaPlayer.play()
        }
    }
    fun startPlayback() {
        initSave {mediaPlayer.play()}
    }
    fun pausePlayback(){
        initSave {mediaPlayer.pause()}
    }
    fun stopPlayback() {
        initSave {mediaPlayer.stop()}
    }
    fun setPlayerVolume(value: Double) {
        initSave {mediaPlayer.volume = value}
    }
    fun seekInPlayback(ms: Int) {
        initSave {mediaPlayer.seek(Duration.millis(ms.toDouble()))}
    }
    fun currentTimeInMillis(): Double? {
        return try {
            mediaPlayer.currentTime.toMillis()
        } catch (e: UninitializedPropertyAccessException) {
             0.0
        }
    }
}
