package com.cv.copper.resources

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.ResourceSupport
import java.util.*

// Author Chris DaMour,  16.09.2014
// https://stackoverflow.com/questions/25858698/spring-hateoas-embedded-resource-support


abstract class HALResource : ResourceSupport() {

    var createdDate: Date? = null
    var lastModifiedDate: Date? = null

    private val embedded = HashMap<String, ResourceSupport>()

    val embeddedResources: Map<String, ResourceSupport>
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        @JsonProperty("_embedded")
        get() = embedded

    fun embed(relationship: String, resource: ResourceSupport?) {
        if (resource != null) {
            embedded[relationship] = resource
        }
    }
}
