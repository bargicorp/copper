package com.cv.copper.player

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.MediaTypes
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.messaging.simp.SimpMessagingTemplate
import kotlinx.coroutines.*

@RestController
@RequestMapping("", produces = [MediaTypes.HAL_JSON_VALUE])
class PlayerController {
    @Autowired
    private lateinit var playerService: PlayerService
    @Autowired
    private lateinit var playerResourceAssembler: PlayerResourceAssembler
    @Autowired
    private lateinit var simpMessagingTemplate: SimpMessagingTemplate

    @PatchMapping("/player")
    fun patch(@RequestBody dto: PlayerDto): ResponseEntity<PlayerResource> {
        val player = playerService.update(dto)
        Thread.sleep(50)
        player?.let {return ResponseEntity.ok(playerResourceAssembler.toResource(player))} ?: return ResponseEntity.notFound().build()
    }
    @GetMapping("/player")
    fun findPlayer() : ResponseEntity<PlayerResource> {
        if (playerService.findById(id) == null) playerService.create(PlayerDto(id))
        playerService.findById(id)?.let { return ResponseEntity.ok(playerResourceAssembler.toResource(it))} ?: return ResponseEntity.notFound().build()
    }
    @PostMapping("/player")
    fun create(@RequestBody dto: PlayerDto): ResponseEntity<PlayerResource> {
        playerService.findById(id)?.let { return ResponseEntity.badRequest ().build()} ?: return ResponseEntity.ok(playerResourceAssembler.toResource(playerService.create(dto)))
    }
    @MessageMapping("/push")
    @SendTo("/updates/player")
    fun push(player: Player) : Player {
        simpMessagingTemplate.convertAndSend("/updates/player", player.id)
        return player
    }

    @MessageMapping("/push/player/tracks")
    @SendTo("/updates/player/tracks")
    fun pushTracks(player: Player) : Player {
        val id = player.id
        GlobalScope.launch {
            delay(500L)
            simpMessagingTemplate.convertAndSend("/updates/player/tracks", id)
        }
        return player
    }
    companion object {
        const val id = 0
    }

}
