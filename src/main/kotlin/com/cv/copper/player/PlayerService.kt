package com.cv.copper.player

import com.cv.copper.track.Track
import java.io.Serializable

interface PlayerService : Serializable {
    fun create(dto: PlayerDto) : Player
    fun update(dto: PlayerDto) : Player?
    fun play(track: Track)
    fun next()
    fun addTrack(trackId: String, index: Int? = null) : Track?
    fun moveTrack(index: Int, targetIndex: Int) : Track?
    fun findById(id: Int): Player?
}