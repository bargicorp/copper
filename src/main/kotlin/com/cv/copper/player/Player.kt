package com.cv.copper.player

import com.cv.copper.Auditable
import com.cv.copper.playlist.Playlist
import com.cv.copper.resources.HALResource
import com.cv.copper.track.Track
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.time.Year
import javax.persistence.*


@Entity
@Table(name = "Player")
data class Player (
        @Id
        var id: Int,
        @Column
        var volume: Double = 0.5,
        @Column(name = "track_list_index")
        var index: Int = 0,
        @Column
        var time: Int = 0,
        @Column
        var paused: Boolean? = false,
        @Column
        @ManyToMany
        @OrderColumn
        var tracks: List<Track>? = mutableListOf()
) : Auditable(), Serializable {
        enum class Action {
                STOP, PLAY, PAUSE, SEEK, PREVIOUS, NEXT

        }
        companion object {
                class Controls(
                        @JsonProperty(value = "player_actions")
                        var actions: Array<Player.Action> = Action.values()
                ): HALResource()
        }
}
