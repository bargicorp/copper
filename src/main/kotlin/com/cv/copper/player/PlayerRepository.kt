package com.cv.copper.player

import org.springframework.data.jpa.repository.JpaRepository

interface PlayerRepository : JpaRepository<Player, String> {
    fun findById(id: Int) : Player?
}
