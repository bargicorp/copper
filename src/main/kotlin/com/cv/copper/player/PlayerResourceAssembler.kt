package com.cv.copper.player

import com.cv.copper.application.MediaPlayerApplication
import com.cv.copper.track.TrackController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.core.DummyInvocationUtils.methodOn
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.hateoas.mvc.ResourceAssemblerSupport
import org.springframework.stereotype.Component

@Component
class PlayerResourceAssembler : ResourceAssemblerSupport<Player, PlayerResource>(Player::class.java, PlayerResource::class.java) {

    @Autowired
    private lateinit var mediaPlayerApp: MediaPlayerApplication

    override fun toResource(player: Player): PlayerResource {
        val resource = PlayerResource(
                player.id,
                player.volume,
                player.index,
                mediaPlayerApp.currentTimeInMillis()?.toInt(),
                player.paused
        )
        resource.createdDate = player.createdDate
        resource.lastModifiedDate = player.lastModifiedDate
        resource.add(linkTo(methodOn(TrackController::class.java).findPlayerTracks()).withRel("tracks"))
        return resource
    }
}
