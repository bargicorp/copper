package com.cv.copper.player

import com.cv.copper.track.Track

data class PlayerDto (

        var id: Int,
        var volume: Double? = null,
        var index: Int? = null,
        var time: Int? = null,
        var trackIds: List<String>? = null,
        var paused: Boolean? = false

    )