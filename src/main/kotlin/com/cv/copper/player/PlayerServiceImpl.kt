package com.cv.copper.player

import com.cv.copper.application.MediaPlayerApplication
import com.cv.copper.track.Track
import com.cv.copper.track.TrackService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.lang.IndexOutOfBoundsException
import java.time.Instant
import java.util.*


@Service
@Transactional
class PlayerServiceImpl : PlayerService {

    @Autowired
    private lateinit var trackService: TrackService
    @Autowired
    private lateinit var playerRepository: PlayerRepository
    @Autowired
    private lateinit var playerController: PlayerController
    @Autowired
    private lateinit var mediaPlayerApp: MediaPlayerApplication

    override fun create(dto: PlayerDto): Player {
        val tracks = dto.trackIds?.let {trackService.buildTrackList(it)}
        val player = Player(
                dto.id,
                dto.volume ?: 0.5,
                0,
                0,
                dto.paused,
                tracks
        )
        return playerRepository.save(player)
    }
    override fun findById(id: Int): Player? {
        return playerRepository.findById(id)
    }

    override fun update(dto: PlayerDto): Player? {
        var player = findById(PlayerController.id)
        player?.let {p -> dto.trackIds?.let {p.tracks = trackService.buildTrackList(it)}
            dto.index?.let {
                    try {
                        p.tracks?.get(it)?.let { track -> play(track)
                            p.index = it
                        }
                    } catch (ex: IndexOutOfBoundsException) {
                        println("Playback index ($it) is out of bounds. Set to 0.")
                    }
                }
            dto.time?.let {
                p.time = it
                mediaPlayerApp.seekInPlayback(it)
            }
            dto.volume?.let {
                p.volume = it
                mediaPlayerApp.setPlayerVolume(it)
            }
            dto.paused?.let {
                p.paused = it
                if (it) {
                    if (dto.time == 0) {
                        p.index = -1
                        mediaPlayerApp.stopPlayback()
                    } else {
                        mediaPlayerApp.pausePlayback()
                    }
                } else {
                    if (p.index == -1) {
                        p.tracks?.get(0)?.let{
                            track -> play(track)
                            p.index = 0
                        }
                    }
                    mediaPlayerApp.startPlayback()
                }
            }
            if (dto.trackIds == null) {
                playerController.push(playerRepository.save(p))
            } else {
                playerController.pushTracks(playerController.push(playerRepository.save(p)))
            }
        }
        return player
    }
    override fun next() {
        val player = findById( PlayerController.id)
        player?.let{
            it.index++
            try {
                play(it.tracks?.get(it.index) ?: return)
            } catch (ex: IndexOutOfBoundsException) {return}
            playerController.push(playerRepository.save(it))
        }
    }
    override fun play(track: Track) {
        // track.playCount?.let { track.playCount = it+1 }
        track.playCount++
        track.lastPlayed = Date.from(Instant.now())
        trackService.update(track)
        mediaPlayerApp.playTrack(track)
    }
    override fun addTrack(trackId: String, index: Int?): Track? {
        val track = trackService.findById(trackId)
        val player = findById(PlayerController.id)
        if (player == null || track == null) return null
        val tracks = mutableListOf<Track>()
        player.tracks?.let {tracks.addAll(it)}
        index?.let {
            tracks.add(it, track)
            if (it <= player.index) player.index++
        } ?: tracks.add(track)
        player.tracks = tracks
        playerController.push(playerRepository.save(player))
        track.trackNo = index
        return track
    }

    override fun moveTrack(index: Int, targetIndex: Int): Track? {
        findById(PlayerController.id)?.let {
            val tracks = mutableListOf<Track>()
            it.tracks?.forEach{track -> tracks.add(track)}
            val track = tracks.removeAt(index)
            tracks.add(targetIndex, track)
            if (index == it.index) {
                it.index = targetIndex
            } else if (index < it.index) {
                    if (targetIndex >= it.index) it.index--
            } else if (index > it.index) {
                if (targetIndex <= it.index) it.index++
            }
            it.tracks = tracks
            playerController.push(playerRepository.save(it))
            return track
        } ?: return null
    }
}
