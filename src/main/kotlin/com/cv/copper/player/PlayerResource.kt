package com.cv.copper.player

import com.cv.copper.resources.HALResource
import com.fasterxml.jackson.annotation.JsonProperty
import javafx.scene.media.Media
import javafx.scene.media.MediaPlayer
import org.springframework.hateoas.core.Relation

@Relation(collectionRelation = "players")
class PlayerResource(

        @JsonProperty(value = "id")
        var pid: Int?,
        var volume: Double?,
        var index: Int?,
        var time: Int?,
        var paused: Boolean?

): HALResource()