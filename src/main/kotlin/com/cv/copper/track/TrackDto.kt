package com.cv.copper.track

import com.cv.copper.artist.Artist
import com.cv.copper.release.Release
import java.io.Serializable
import java.time.Duration

data class TrackDto (

        var id: String? = null,
        var title: String? = null,
        var duration: Duration? = null,
        var filepath: String? = null,
        var version: Track.Version? = null,
        var trackNo: Int? = null,
        var artist: List<Artist>? = mutableListOf(),
        var release: Release? = null

        ): Serializable
