package com.cv.copper.track

import com.cv.copper.artist.Artist
import com.cv.copper.artist.ArtistService
import com.cv.copper.release.ReleaseService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service
@Transactional
class TrackServiceImpl : TrackService {

    @Autowired
    private lateinit var trackRepository: TrackRepository
    @Autowired
    private lateinit var artistService: ArtistService
    @Autowired
    private lateinit var releaseService: ReleaseService

    override fun findAll(p: Pageable): Page<Track> {
        var tracks: Page<Track> = trackRepository.findAll(p)
        return trackRepository.findAll(p)
    }

    override fun findAll(p: Pageable, query: String): Page<Track> {
        return trackRepository.findAllByTitleContainsOrArtists_NameContainsOrRelease_NameContains(p, query, query, query)
    }

    override fun findById(id: String): Track? {
        return trackRepository.findById(id).orElse(null)
    }

    override fun findByArtist(artistId: String, p: Pageable?): Page<Track> {
        return artistService.findById(artistId)?.let {
            trackRepository.findAllByArtists(it, p)
        } ?: Page.empty()
    }

    override fun findByArtist(artistId: String, p: Pageable?, query: String): Page<Track> {
        return artistService.findById(artistId)?.let {
            return trackRepository.findAllByTitleContainsAndArtists(query, it, p)
        } ?: Page.empty()
    }

    override fun findByReleaseId(releaseId: String): List<Track?> {
        return releaseService.findById(releaseId)?.let{
            trackRepository.findByReleaseOrderByTrackNo(it)
        } ?: listOf<Track>()
    }

    override fun create(dto: TrackDto): Track {
        val track = Track(
                "",
                dto.title,
                dto.duration,
                null,
                dto.trackNo,
                mutableListOf(),
                null,
                null,
                null
                )
        return trackRepository.save(track)

    }

    /**
     * This method checks if a track already exists before saving it
     *
     * @return the existing track, if exists or the new one
     */
    override fun createSave(track: Track): Track?{
        //TODO replace with a function that checks for more than one artist
        val artist = track.artists?.let {listOf(it[0])}
        if (trackRepository.findByTitleAndArtistsAndRelease(track.title, artist, track.release) == null) {
            trackRepository.save(track)
        }
        trackRepository.findByTitleAndArtistsAndRelease(track.title, artist, track.release)?.let {return null}
        return trackRepository.save(track)
        /*var response: Track? = track
        track.release?.let {
            {
                track.artists?.let {
                    var counter = 0
                    for (artist in it) {
                        if (findByTitleAndArtistsAndRelease(track) != null) counter++
                    }
                    if (counter == it.size) {
                        println("Track with same Name, Artists and Release already exists in database")
                        response = null
                    }
                    // TODO Add a check-routine for tracks that have no artists but a release and name set
                }
            }
        } // TODO Add a check-routine for tracks that have no release set
        response?.let {return trackRepository.save(track)} ?: return null*/
    }

    override fun update(track: Track): Track {
        return trackRepository.save(track)

    }
    override fun buildTrackList(ids: List<String>): List<Track> {
        val list = mutableListOf<Track>()
        for (id in ids){
            findById(id)?.let {list.add(it)}
        }
        return list
    }

    override fun findByTitleAndArtistsAndRelease(track: Track): Track? {
        return trackRepository.findByTitleAndArtistsAndRelease(track.title, track.artists, track.release)
    }

    override fun findRandom(p: Pageable, amount: Long): Page<Track> {
        val list = trackRepository.findRandom(amount)
        return PageImpl<Track>(list, p, list.size.toLong())
    }
}
