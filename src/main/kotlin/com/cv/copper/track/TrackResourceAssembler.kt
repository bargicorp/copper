package com.cv.copper.track

import com.cv.copper.artist.ArtistResourceAssembler
import com.cv.copper.release.ReleaseResourceAssembler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.mvc.ResourceAssemblerSupport
import org.springframework.stereotype.Component

@Component
class TrackResourceAssembler : ResourceAssemblerSupport<Track, TrackResource>(Track::class.java, TrackResource::class.java) {

    @Autowired
    private lateinit var artistResourceAssembler: ArtistResourceAssembler
    @Autowired
    private lateinit var releaseResourceAssembler: ReleaseResourceAssembler
    override fun toResource(track: Track): TrackResource {

        val resource = TrackResource(
                track.id,
                track.title,
                track.duration?.seconds,
                track.playCount
        )
        resource.createdDate = track.createdDate
        resource.lastModifiedDate = track.lastModifiedDate
        track.artists?.let {
            val relString = if (it.size > 1) "artists" else "artist"
            for (artist in it) resource.embed(relString, artistResourceAssembler.toResource(artist))
        }
        track.release?.let {
            resource.embed("release", releaseResourceAssembler.toResource(it))
        }
        return resource
    }
}
