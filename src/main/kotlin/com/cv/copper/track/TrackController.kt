package com.cv.copper.track

import com.cv.copper.artist.ArtistResource
import com.cv.copper.player.PlayerController
import com.cv.copper.player.PlayerService
import com.cv.copper.playlist.PlaylistService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.GetMapping

@RestController
@RequestMapping("", produces = [MediaTypes.HAL_JSON_VALUE])
class TrackController {

    @Autowired
    private lateinit var trackResourceAssembler: TrackResourceAssembler
    @Autowired
    private lateinit var trackService: TrackService
    @Autowired
    private lateinit var playerService: PlayerService
    @Autowired
    private lateinit var playlistService: PlaylistService

    @GetMapping("/tracks")
    fun findAll(@RequestParam(value = "query", required = false) query: String?, pageable: Pageable, assembler: PagedResourcesAssembler<Track>): PagedResources<TrackResource> {
        val trackPage = if (query == null) trackService.findAll(pageable) else trackService.findAll(pageable, query)
        return assembler.toResource(trackPage, trackResourceAssembler)
    }
    @PostMapping("/tracks")
    fun create(@RequestBody dto: TrackDto): ResponseEntity<TrackResource> {
        return ResponseEntity.ok(trackResourceAssembler.toResource(trackService.create(dto)))
    }
    @GetMapping("/tracks/{trackId}")
    fun findById(@PathVariable("trackId") trackId: String): ResponseEntity<TrackResource> {
        val track = trackService.findById(trackId)
        track?.let { return ResponseEntity.ok(trackResourceAssembler.toResource(track))} ?: return ResponseEntity.notFound().build()
    }
    @GetMapping("/artists/{artistId}/tracks")
    fun findByArtist(@PathVariable("artistId") artistId: String, @RequestParam(value = "query", required = false) query: String?, pageable: Pageable?, assembler: PagedResourcesAssembler<Track>?): PagedResources<TrackResource>? {
        val trackPage = if (query == null) trackService.findByArtist(artistId, pageable) else trackService.findByArtist(artistId, pageable, query)
        return assembler?.toResource(trackPage, trackResourceAssembler)
    }
    @GetMapping("/releases/{releaseId}/tracks")
    fun findByRelease(@PathVariable("releaseId") releaseId: String): ResponseEntity<List<TrackResource>> {
        val tracks = trackService.findByReleaseId(releaseId)
        val resources = mutableListOf<TrackResource>()
        for (track in tracks) track?.let {resources.add(trackResourceAssembler.toResource(it))}
        return ResponseEntity.ok(resources)
    }
    @GetMapping("/player/tracks")
    fun findPlayerTracks() : ResponseEntity<List<TrackResource>> {
        val tracks = playerService.findById(PlayerController.id)?.tracks
        val resources = mutableListOf<TrackResource>()
        tracks?.let {for (track in tracks) resources.add(trackResourceAssembler.toResource(track))}
        return ResponseEntity.ok(resources)
    }
    @PostMapping("/player/tracks")
    fun addTrack(@RequestBody dto: TrackDto) : ResponseEntity<TrackResource> {
        var track: Track?
        dto.id?.let {
            track = if (dto.trackNo == null) playerService.addTrack(it) else playerService.addTrack(it, dto.trackNo)
            track?.let { track ->
                return ResponseEntity.ok(trackResourceAssembler.toResource(track))
            }
        }
        return ResponseEntity.badRequest().build()
    }
    @PatchMapping("/player/tracks/{index}")
    fun movePlayerTrack(@PathVariable index: String, @RequestBody dto: TrackDto) : ResponseEntity<TrackResource> {
        var track: Track?
        dto.trackNo?.let { track = playerService.moveTrack(Integer.parseInt(index), it)
            track?.let {track -> return ResponseEntity.ok(trackResourceAssembler.toResource(track))
            }
        }
        return ResponseEntity.badRequest().build()
    }
    @GetMapping("/playlists/{playlistId}/tracks")
    fun findPlaylistTracks(@PathVariable playlistId: String) : ResponseEntity<List<TrackResource>> {
        val tracks = playlistService.findById(playlistId)?.tracks
        val resources = mutableListOf<TrackResource>()
        tracks?.let {for (track in tracks) resources.add(trackResourceAssembler.toResource(track))}
        return ResponseEntity.ok(resources)
    }
    @PostMapping("/playlists/{playlistId}/tracks")
    fun addTrackToPlaylist(@PathVariable playlistId: String, @RequestBody dto: TrackDto) : ResponseEntity<TrackResource> {
        var track: Track?
        dto.id?.let {
            track = if (dto.trackNo == null) playlistService.addTrack(playlistId, it) else playlistService.addTrack(playlistId, it, dto.trackNo)
            track?.let { track ->
                return ResponseEntity.ok(trackResourceAssembler.toResource(track))
            }
        }
        return ResponseEntity.badRequest().build()
    }
    @PatchMapping("/playlists/{playlistId}/tracks/{index}")
    fun movePlaylistTracks(@PathVariable index: Int, @PathVariable playlistId: String, @RequestBody dto: TrackDto) : ResponseEntity<TrackResource> {
        var track: Track?
        dto.trackNo?.let { track = playlistService.moveTrack(playlistId, index, it)
            track?.let {track -> return ResponseEntity.ok(trackResourceAssembler.toResource(track))
            }
        }
        return ResponseEntity.badRequest().build()
    }
    @GetMapping("/tracks/random/{number}")
    fun findRandom(@PathVariable("number") number: Long, pageable: Pageable, assembler: PagedResourcesAssembler<Track>): PagedResources<TrackResource> {
        //return trackResourceAssembler.toResources(trackService.findRandom(number))
        val trackPage = trackService.findRandom(pageable, number)
        return assembler.toResource(trackPage, trackResourceAssembler)
    }
}
