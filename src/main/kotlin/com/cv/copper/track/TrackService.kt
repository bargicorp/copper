package com.cv.copper.track

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.io.Serializable

interface TrackService : Serializable {
    fun findAll(p: Pageable): Page<Track>
    fun findAll(p: Pageable, query: String): Page<Track>
    fun findById(id: String): Track?
    fun findByArtist(artistId: String, p: Pageable?) : Page<Track>
    fun findByArtist(artistId: String, p: Pageable?, query: String) : Page<Track>
    fun findByReleaseId(releaseId: String) : List<Track?>
    fun create(dto: TrackDto): Track
    fun createSave(track: Track): Track?
    fun update(track: Track): Track
    fun buildTrackList(ids: List<String>): List<Track>
    fun findByTitleAndArtistsAndRelease(track: Track): Track?
    fun findRandom(p: Pageable, amount: Long): Page<Track>

}

