package com.cv.copper.track

import com.cv.copper.Auditable
import com.cv.copper.artist.Artist
import com.cv.copper.file.FileEntity
import com.cv.copper.player.Player
import com.cv.copper.release.Release
import org.hibernate.annotations.GenericGenerator
import org.springframework.lang.NonNull
import java.io.Serializable
import java.time.Duration
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "Tracks")
data class Track(
        @Id
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
        @Column(length = 36, name = "track_id")
        var id: String,
        @Column(name = "title")
        var title: String?,
        var duration: Duration?,
        var version: Track.Version? = Track.Version.ORIGINAL,
        var trackNo: Int?,
        @OneToMany
        var files: List<FileEntity?> = mutableListOf(),
        @ManyToMany
        var artists: MutableList<Artist>? = mutableListOf(),
        @ManyToOne
        var release: Release?,
        @ManyToMany(mappedBy = "tracks")
        var players: List<Player>? = mutableListOf(),
        @NonNull
        var playCount: Int = 0,
        @Temporal(TemporalType.TIMESTAMP)
        var lastPlayed: Date? = null
)  : Auditable(), Serializable {
        enum class Version {
                ORIGINAL, CLONE, COVER, LIVE, REMIX
        }
        override fun toString(): String {
                return "Track(id=$id, title=$title, artists=$artists, trackNo=$trackNo)"
        }

}
