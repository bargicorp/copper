package com.cv.copper.track
import com.cv.copper.artist.Artist
import com.cv.copper.release.Release
import org.eclipse.persistence.jpa.jpql.parser.Expression.SELECT
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface TrackRepository : JpaRepository<Track, String> {

    fun findAllByTitleContains(p: Pageable, query: String): Page<Track>
    fun findByTitleAndArtistsAndRelease(title: String?, artists: List<Artist>?, release: Release?): Track?
    fun findByReleaseOrderByTrackNo(release: Release): List<Track?>
    fun findAllByTitleContainsAndArtists(query: String, artist: Artist, p: Pageable?): Page<Track>
    fun findAllByArtists(artist: Artist, p: Pageable?): Page<Track>
    fun countAllByArtists(artist: Artist): Long
    fun findAllByTitleContainsOrArtists_NameContainsOrRelease_NameContains(p: Pageable, query: String, query2: String, query3: String): Page<Track>
    @Query("SELECT * FROM db_copper.tracks ORDER BY RAND() LIMIT ?1 ;", nativeQuery = true)
    fun findRandom(number: Long): List<Track>
}
