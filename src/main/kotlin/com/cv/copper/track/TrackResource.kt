package com.cv.copper.track

import com.cv.copper.release.Release
import com.cv.copper.resources.HALResource
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.core.Relation
import java.time.Duration
import java.util.*

@Relation(collectionRelation = "tracks")
data class TrackResource(

        @JsonProperty(value = "id")
        var eid: String?,
        var title: String?,
        var duration: Long?,
        var playCount: Int?
        ): HALResource()
