package com.cv.copper.playlist

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.http.ResponseEntity
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/playlists", produces = [MediaTypes.HAL_JSON_VALUE])
class PlaylistController {

    @Autowired
    private lateinit var playlistResourceAssembler: PlaylistResourceAssembler
    @Autowired
    private lateinit var playlistService: PlaylistService
    @Autowired
    private lateinit var simpMessagingTemplate: SimpMessagingTemplate

    @PostMapping("")
    fun create(@RequestBody dto: PlaylistDto): ResponseEntity<PlaylistResource> {
        return ResponseEntity.ok(playlistResourceAssembler.toResource(playlistService.create(dto.name)))

    }
    @GetMapping("")
    fun findAll(@RequestParam(value = "query", required = false) query: String?, pageable: Pageable, assembler: PagedResourcesAssembler<Playlist>): PagedResources<PlaylistResource> {
        val playlistPage = if (query == null) playlistService.findAll(pageable) else playlistService.findAll(pageable, query)
        return assembler.toResource(playlistPage, playlistResourceAssembler)
    }
    @GetMapping("/{playlistId}")
    fun findById(@PathVariable("playlistId") playlistId: String): ResponseEntity<PlaylistResource> {
        val playlist = playlistService.findById(playlistId)
        playlist?.let { return ResponseEntity.ok(playlistResourceAssembler.toResource(playlist))} ?: return ResponseEntity.notFound().build()
    }
    @PatchMapping("/{playlistId}")
    fun update(@RequestBody dto: PlaylistDto, @PathVariable("playlistId") playlistId: String): ResponseEntity<PlaylistResource> {
        val playlist = playlistService.update(playlistId, dto)
        playlist?.let { return ResponseEntity.ok(playlistResourceAssembler.toResource(playlist))} ?: return ResponseEntity.notFound().build()
    }
    @DeleteMapping("/{playlistId}")
    fun delete(@PathVariable("playlistId") playlistId: String): ResponseEntity<PlaylistResource> {
        playlistService.delete(playlistId)
        return ResponseEntity.noContent().build()
    }
    @MessageMapping("/push/playlists")
    @SendTo("/updates/playlists")
    fun push(playlist: Playlist) : Playlist {
        simpMessagingTemplate.convertAndSend("/updates/playlists", playlist.id?: 0)
        return playlist
    }
}
