package com.cv.copper.playlist

import com.cv.copper.track.Track
import com.cv.copper.track.TrackService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Service
@Transactional
class PlaylistServiceImpl : PlaylistService {
    @Autowired
    private lateinit var trackService: TrackService
    @Autowired
    private lateinit var playlistRepository: PlaylistRepository
    @Autowired
    private lateinit var playlistController: PlaylistController

    override fun findAll(p: Pageable): Page<Playlist> {
        return playlistRepository.findAll(p)
    }

    override fun findAll(p: Pageable, query: String): Page<Playlist> {
        return playlistRepository.findAllByNameContains(p, query)
    }

    override fun findById(id: String): Playlist? {
        return playlistRepository.findById(id).orElse(null)
    }

    override fun create(name: String?): Playlist {
        return playlistRepository.save(Playlist(name = name ?: "Playlist #" + playlistRepository.findAll().size))
    }

    override fun addTrack(playlistId: String, trackId: String, index: Int?): Track? {
        val track = trackService.findById(trackId)
        val playlist = findById(playlistId)
        if (playlist == null || track == null) return null
        val tracks = mutableListOf<Track>()
        playlist.tracks?.let {tracks.addAll(it)}
        index?.let {
            tracks.add(it, track)
        } ?: tracks.add(track)
        playlist.tracks = tracks
        playlistController.push(playlistRepository.save(playlist))
        track.trackNo = index
        return track
    }
    override fun update(playlistId: String, dto: PlaylistDto): Playlist? {
        findById(playlistId)?.let {pl ->
            dto.name?.let { pl.name = it}
            dto.trackIds?.let { pl.tracks = trackService.buildTrackList(it) }
            return playlistController.push(playlistRepository.save(pl))
        } ?: return null
    }
    override fun moveTrack(playlistId: String, index: Int, targetIndex: Int): Track? {
        findById(playlistId)?.let {
            val tracks = mutableListOf<Track>()
            it.tracks?.forEach{track -> tracks.add(track)}
            val track = tracks.removeAt(index)
            tracks.add(targetIndex, track)
            it.tracks = tracks
            playlistController.push(playlistRepository.save(it))
            return track
        } ?: return null
    }

    override fun delete(playlistId: String) {
        return playlistRepository.deleteById(playlistId)
    }
}
