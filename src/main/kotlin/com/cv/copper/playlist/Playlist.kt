package com.cv.copper.playlist

import com.cv.copper.Auditable
import com.cv.copper.track.Track
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
@Table(name = "Playlists")
data class Playlist(
        @Id
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
        @Column(length = 36)
        var id: String? = null,
        @Column
        var name: String? = null,
        @Column
        @ManyToMany
        @OrderColumn
        var tracks: List<Track>? = mutableListOf(),
        @Column
        @GeneratedValue
        var creationIndex: Long? = null

) :  Auditable()

