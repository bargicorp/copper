package com.cv.copper.playlist

import com.cv.copper.track.Track
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.io.Serializable

interface PlaylistService : Serializable {
    fun findAll(p: Pageable): Page<Playlist>
    fun findAll(p: Pageable, query: String): Page<Playlist>
    fun findById(id: String): Playlist?
    fun create(name: String?): Playlist
    fun update(playlistId: String, dto: PlaylistDto): Playlist?
    fun addTrack(playlistId: String, trackId: String, index: Int? = null) : Track?
    fun moveTrack(playlistId: String, index: Int, targetIndex: Int) : Track?
    fun delete(playlistId: String)
}

