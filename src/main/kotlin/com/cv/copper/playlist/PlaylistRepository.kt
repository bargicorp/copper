package com.cv.copper.playlist

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface PlaylistRepository : JpaRepository<Playlist, String> {

    fun findAllByNameContains(p: Pageable, query: String): Page<Playlist>
}