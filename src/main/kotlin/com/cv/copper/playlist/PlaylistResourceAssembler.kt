package com.cv.copper.playlist

import com.cv.copper.track.TrackController
import com.cv.copper.track.TrackResourceAssembler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.mvc.ControllerLinkBuilder
import org.springframework.hateoas.mvc.ResourceAssemblerSupport
import org.springframework.stereotype.Component

@Component
class PlaylistResourceAssembler : ResourceAssemblerSupport<Playlist, PlaylistResource>(Playlist::class.java, PlaylistResource::class.java) {

    @Autowired
    private lateinit var trackResourceAssembler: TrackResourceAssembler

    override fun toResource(playlist: Playlist): PlaylistResource {
        val resource = PlaylistResource(
                playlist.id,
                playlist.name
        )
        resource.createdDate = playlist.createdDate
        resource.lastModifiedDate = playlist.lastModifiedDate
        playlist.id?.let {
            resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(PlaylistController::class.java).findById(it)).withRel("self"))
            resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(TrackController::class.java).findPlaylistTracks(it)).withRel("tracks"))
        }
        return resource
    }
}
