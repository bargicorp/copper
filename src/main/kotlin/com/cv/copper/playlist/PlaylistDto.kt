package com.cv.copper.playlist

import java.io.Serializable

data class PlaylistDto (
        var id: String? = null,
        var name: String? = null,
        var trackIds: List<String>? = null
    ): Serializable