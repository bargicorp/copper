package com.cv.copper.playlist

import com.cv.copper.resources.HALResource
import com.cv.copper.track.Track
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.annotation.CreatedDate
import org.springframework.hateoas.core.Relation
import java.util.*

@Relation(collectionRelation = "playlists")
data class PlaylistResource(

        @JsonProperty(value = "id")
        var lid: String?,
        var name: String?
        ) : HALResource()
